syntax on
"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath^=~/.vim/repos/github.com/Shougo/dein.vim

" Required:
call dein#begin(expand('~/.vim'))

" Let dein manage dein
" Required:
call dein#add('Shougo/dein.vim')

" Add or remove your plugins here:
call dein#add('scrooloose/nerdtree')
call dein#add('vim-airline/vim-airline')
call dein#add('vim-airline/vim-airline-themes')
call dein#add('jmcantrell/vim-virtualenv')
call dein#add('Shougo/deoplete.nvim')
call dein#add('tpope/vim-fugitive')
call dein#add('mattn/emmet-vim')
call dein#add('neomake/neomake')
call dein#add('roosta/vim-srcery')
call dein#add('majutsushi/tagbar')
call dein#add('rust-lang/rust.vim')
call dein#add('sheerun/vim-polyglot')
call dein#add('slashmili/alchemist.vim')
call dein#add('mhinz/neovim-remote')

" Required:
call dein#end()

" Required:
filetype plugin indent on

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif

"End dein Scripts-------------------------

"Switch leader to spacebar
let mapleader = ' '

" Emmet customizations
imap <C-y><space> <plug>(emmet-expand-abbr)
if has('terminal')
  " Fix terminal keybinds
  tnoremap <C-h> <C-\><C-N><C-w>h
  tnoremap <C-j> <C-\><C-N><C-w>j
  tnoremap <C-k> <C-\><C-N><C-w>k
  tnoremap <C-l> <C-\><C-N><C-w>l

  " Start terminals in insert mode
  autocmd TermOpen * startinsert

  " Make status line sane
  autocmd TermOpen * let w:airline_section_c='%{b:term_title}'

  " Escape term mode
  tnoremap <ESC> <C-\><C-n>

  " If no files open, open console
  autocmd StdinReadPre * let s:std_in=1
  if argc() == 0 && !exists("s:std_in")
    term
    let w:airline_section_c='%{b:term_title}'
    startinsert
  endif
endif


" Sane pane switch bindings
inoremap <C-h> <C-\><C-N><C-w>h
inoremap <C-j> <C-\><C-N><C-w>j
inoremap <C-k> <C-\><C-N><C-w>k
inoremap <C-l> <C-\><C-N><C-w>l
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l


" Set shell to zsh
let &shell = "/usr/bin/zsh"

" Use nvim remote from within nvim
if has('nvim')
  let $VISUAL = 'nvr -cc split --remote-wait'
endif

" NerdTree config stuff
map <leader>nn :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen=1

" Tagbar config stuff
map <leader>tt :TagbarToggle<CR>
let g:tagbar_autofocus = 1

" Powerline config stuff
let g:airline_powerline_fonts=1

" Fix tab stuff
set tabstop=2
set shiftwidth=2
set expandtab

" Set a pretty color scheme
colorscheme srcery

" Use system clipboard by default
set clipboard=unnamedplus

" Function to check if a file is in a local git repo
function IsFileInGit()
  silent exec "!(cd %:p:h; git rev-parse --git-dir 2>&1 /dev/null)"
  redraw!
  return v:shell_error
endfunction

" If file is git controlled, trigger neomake
autocmd! BufWritePost * if IsFileInGit() == 0 | Neomake | endif
autocmd! BufRead      * if IsFileInGit() == 0 | Neomake | endif

" Fix for some file encoding issues
set fileencodings=utf8,latin1,utf16

